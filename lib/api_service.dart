import 'dart:convert';

import 'package:http/http.dart';
import 'package:wheather_app/models/wheather_model.dart';

class ApiService {
  ApiService._privateConstructor();

  static final ApiService _instance = ApiService._privateConstructor();

  static ApiService get instance => _instance;

  Future<WheatherDataModel> getWheather(String lat, String lon) async {
    try {
      Response response = await get(Uri.parse(
          'https://api.openweathermap.org/data/3.0/onecall?lat=$lat&lon=$lon&exclude=minutely,alerts&units=metric&appid=39b1162f8622dbdc1b917027335a35a6'));

      if (response.statusCode == 200) {
        return WheatherDataModel.fromJson(jsonDecode(response.body));
      } else {
        throw 'Response code: ${response.statusCode}.';
      }
    } catch (e) {
      throw 'Error. $e';
    }
  }
}
