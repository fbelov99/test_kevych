import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stacked/stacked.dart';
import 'package:wheather_app/home_page/home_page_vm.dart';
import 'package:wheather_app/models/wheather_model.dart';

import '../main.dart';
import '../widget_helper/custom_scaffold.dart';
import '../widget_helper/custom_text_form.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomePageController>.reactive(
        viewModelBuilder: () => HomePageController(),
        onViewModelReady: (viewModel) => viewModel.getPosition(context),
        builder: (context, model, child) {
          return CustomScaffold(
            isBusy: model.isBusy,
            body: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: size.width * .03, vertical: size.height * .03),
              child: Column(
                children: [
                  TextFormCustom(
                    borderColor: Colors.grey.shade100,
                    fillColor: Colors.grey,
                    fullBorder: true,
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        model.autoCompleteSearch(value);
                      } else {
                        if (model.predictions.isNotEmpty) {
                          model.predictions = [];
                        }
                      }
                      model.notifyListeners();
                    },
                    controller: model.placeContr,
                    hintText: model.selectedPred?.description ??
                        // model.placemark?.first.name ??
                        model.placemark?.locality ??
                        "Input City",
                    keyboardType: TextInputType.text,
                    suffix: IconButton(
                      onPressed: () {
                        model.placeContr.text = '';
                        model.predictions = [];
                        model.notifyListeners();
                      },
                      icon: Icon(
                        Icons.close_outlined,
                        size: size.height * 0.04,
                        color: Colors.grey.shade100,
                      ),
                    ),
                  ),
                  if (model.predictions.isNotEmpty &&
                      model.placeContr.text.isNotEmpty)
                    SizedBox(
                      height: size.height * 0.2,
                      width: size.width * 0.6,
                      child: RawScrollbar(
                        controller: model.placeScroll,
                        thumbColor: Colors.white,
                        thickness: 4,
                        radius: const Radius.circular(2),
                        child: ListView.builder(
                          controller: model.placeScroll,
                          // shrinkWrap: true,
                          // physics:
                          //     const AlwaysScrollableScrollPhysics(),
                          itemCount: model.predictions.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              selected: model.selectedPred ==
                                  model.predictions[index],
                              leading: const CircleAvatar(
                                child: Icon(
                                  Icons.pin_drop,
                                  color: Colors.white,
                                ),
                              ),
                              title: Text(
                                  model.predictions[index].description ?? ''),
                              onTap: () {
                                model.selectedPred = model.predictions[index];
                                model.placeContr.text =
                                    model.predictions[index].description ?? '';
                                model.notifyListeners();
                                model.getPosition(
                                  context,
                                  placeId: model.selectedPred?.placeId,
                                  search: model.selectedPred?.description,
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  if (model.placemarkList.isNotEmpty)
                    Text(
                        '${model.placemarkList.firstOrNull?.locality ?? ''}, ${model.placemarkList.firstOrNull?.administrativeArea ?? ''}, ${model.placemarkList.firstOrNull?.country ?? ''}',
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w400)),
                  if (model.wheatherModel?.current?.weather?.first.icon != null)
                    CachedNetworkImage(
                      imageUrl:
                          'https://openweathermap.org/img/wn/${model.wheatherModel?.current?.weather?.firstOrNull?.icon ?? '11d'}@2x.png',
                      progressIndicatorBuilder: (context, url, progress) =>
                          const CircularProgressIndicator(
                        strokeWidth: 4.0,
                        color: Colors.blue,
                        backgroundColor: Colors.transparent,
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                      ),
                    ),
                  if (model.wheatherModel?.current?.temp?.isNotEmpty ?? false)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.thermostat, color: Colors.white),
                        Text('${model.wheatherModel?.current?.temp ?? '0'}°C',
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.w600)),
                      ],
                    ),
                  if (model.wheatherModel?.hourlyTemp?.isNotEmpty ?? false)
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: size.height * .02),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.thermostat, color: Colors.white),
                          Text(
                              '${(model.wheatherModel?.hourlyTemp?..sort())?.firstOrNull ?? '0'}-${(model.wheatherModel?.hourlyTemp?..sort())?.lastOrNull ?? '0'}°C',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                  if (model.wheatherModel?.current?.humidity?.isNotEmpty ??
                      false)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.water_drop_outlined,
                            color: Colors.white),
                        Text(
                            '${model.wheatherModel?.current?.humidity ?? '0'}%',
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                  SizedBox(
                    height: size.height * .55,
                    child: ListView.builder(
                        itemCount: model.wheatherModel?.daily?.length ?? 0,
                        itemBuilder: (context, i) {
                          if (DateFormat('dd/MMM/yyyy').format(
                                  model.wheatherModel?.daily?[i].dt ??
                                      DateTime.now()) !=
                              DateFormat('dd/MMM/yyyy')
                                  .format(DateTime.now())) {
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: size.height * .005),
                              child: weeklyItem(model.wheatherModel?.daily?[i]),
                            );
                          }
                          return Container();
                        }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget weeklyItem(DailyModel? item) {
    return Row(
      children: [
        SizedBox(
          width: size.width * .25,
          child: Text(
            getWeekDay(item?.dt?.weekday),
            // item?.dt ?? '',
            style: const TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ),
        if (item?.weather?.first.icon != null)
          CachedNetworkImage(
            imageUrl:
                'https://openweathermap.org/img/wn/${item?.weather?.firstOrNull?.icon ?? '11d'}.png',
            progressIndicatorBuilder: (context, url, progress) =>
                const CircularProgressIndicator(
              strokeWidth: 4.0,
              color: Colors.blue,
              backgroundColor: Colors.transparent,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
            ),
          ),
        Padding(
          padding: EdgeInsets.only(left: size.width * .02),
          child: const Icon(Icons.thermostat, color: Colors.red),
        ),
        SizedBox(
          width: size.width * .2,
          child: Text('${item?.temp?.day ?? '0'}°C',
              style: const TextStyle(
                  color: Colors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.w400)),
        ),
        Padding(
          padding: EdgeInsets.only(left: size.width * .02),
          child: const Icon(Icons.water_drop_outlined, color: Colors.blue),
        ),
        Text('${item?.humidity ?? '0'}%',
            style: const TextStyle(
                color: Colors.blue, fontSize: 18, fontWeight: FontWeight.w400)),
      ],
    );
  }

  String getWeekDay(int? day) {
    switch (day) {
      case 2:
        return 'Tuesday';
      case 3:
        return 'Wednesday';
      case 4:
        return 'Thursday';
      case 5:
        return 'Friday';
      case 6:
        return 'Saturday';
      case 7:
        return 'Sunday';
      default:
        return 'Monday';
    }
  }
}
