import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:wheather_app/models/wheather_model.dart';
import 'package:geocoding/geocoding.dart' as geo;
import 'package:geolocator/geolocator.dart';
import 'package:google_place/google_place.dart';

import '../api_service.dart';
import '../widget_helper/toast.dart';

class HomePageController extends BaseViewModel {
  Position? position;
  final TextEditingController placeContr = TextEditingController();
  WheatherDataModel? wheatherModel;
  geo.Placemark? placemark;
  List<geo.Placemark> placemarkList = [];
  AutocompletePrediction? selectedPred;
  final ScrollController placeScroll = ScrollController();

  GooglePlace googlePlace =
      GooglePlace('AIzaSyA-TwKGEoP77NIXfbnFGCyLG7av-21-3Ak');
  List<AutocompletePrediction> predictions = [];

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null) {
      predictions = result.predictions ?? [];
      notifyListeners();
    }
  }

  getPosition(context, {String? search, String? placeId}) async {
    setBusy(true);
    if (search?.isNotEmpty ?? false) {
      notifyListeners();
      placemarkList = [];
      predictions = [];
      try {
        DetailsResponse? resp = await googlePlace.details.get(placeId!);
        if (resp != null) {
          List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
              resp.result?.geometry?.location?.lat ?? 0,
              resp.result?.geometry?.location?.lng ?? 0);
          placemarkList.add(placemarks.first);
          wheatherModel = await ApiService.instance.getWheather(
              (resp.result?.geometry?.location?.lat ?? 0).toString(),
              (resp.result?.geometry?.location?.lng ?? 0).toString());
          notifyListeners();
        }
        notifyListeners();
        Navigator.of(context).pop();
      } catch (e) {
        showToast('Error: $e');
        notifyListeners();
      }
    } else {
      try {
        bool serviceEnabled;
        LocationPermission permission;
        serviceEnabled = await Geolocator.isLocationServiceEnabled();
        if (!serviceEnabled) {
          return '';
        }

        permission = await Geolocator.checkPermission();
        if (permission == LocationPermission.denied) {
          await showToast(
                  "Can't determine your position. Please give the permission to the app")
              .then((value) async {
            permission = await Geolocator.requestPermission();
            if (permission == LocationPermission.denied) {
              return '';
            }
          });
        }

        if (permission == LocationPermission.deniedForever) {
          return '';
        }

        await Geolocator.getCurrentPosition().then((value) async {
          position = value;
          try {
            List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
              value.latitude,
              value.longitude,
            );
            placemarkList.add(placemarks.first);
            // placeContr.text =
            //     '${placemarks.first.locality ?? ''}, ${placemarks.first.administrativeArea ?? ''}, ${placemarks.first.country ?? ''}';
            wheatherModel = await ApiService.instance.getWheather(
                value.latitude.toString(), value.longitude.toString());
            notifyListeners();
          } catch (err) {
            showToast('Error: $err');
            notifyListeners();
          }
        });
      } catch (e) {
        showToast('Error: $e');
      }
    }
    setBusy(false);
    return placemark;
  }
}
