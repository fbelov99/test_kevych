import 'package:intl/intl.dart';

class WheatherDataModel {
  CurrentModel? current;
  List<double>? hourlyTemp;
  List<DailyModel>? daily;

  WheatherDataModel({
    this.current,
    this.hourlyTemp,
    this.daily,
  });

  factory WheatherDataModel.fromJson(Map<String, dynamic> json) {
    return WheatherDataModel(
      current: CurrentModel.fromJson(json['current']),
      hourlyTemp: List.of(json['hourly'] ?? [])
          .map((e) => (e['temp'] as double?) ?? 0)
          .toList(),
      daily: List.of(json['daily'] ?? [])
          .map((e) => DailyModel.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'current': current?.toJson(),
      'hourly': hourlyTemp?.map((e) => {'temp': e}).toList(),
      'daily': daily?.map((e) => e.toJson()).toList(),
    };
  }
}

class CurrentModel {
  String? dt, temp, humidity, summary;
  List<WheatherModel>? weather;

  CurrentModel({
    this.dt,
    this.temp,
    this.humidity,
    this.summary,
    this.weather,
  });

  factory CurrentModel.fromJson(Map<String, dynamic> json) {
    return CurrentModel(
      dt: json['dt'] != null
          ? DateFormat('dd/MMM/yyyy').format(
              DateTime.fromMillisecondsSinceEpoch((json['dt'] ?? 0) * 1000,
                  isUtc: true))
          : json['dt'],
      temp: stringOrNull(json['temp']),
      humidity: stringOrNull(json['humidity']),
      summary: stringOrNull(json['summary']),
      weather: List.of(json['weather'] ?? [])
          .map((e) => WheatherModel.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'dt': dt,
      'temp': temp,
      'humidity': humidity,
      'summary': summary,
      'weather': weather?.map((e) => e.toJson()).toList(),
    };
  }
}

class DailyModel {
  DateTime? dt;
  String? humidity, summary;
  TemperatureModel? temp;
  List<WheatherModel>? weather;

  DailyModel({
    this.dt,
    this.temp,
    this.humidity,
    this.summary,
    this.weather,
  });

  factory DailyModel.fromJson(Map<String, dynamic> json) {
    return DailyModel(
      dt: json['dt'] != null
          ? DateTime.fromMillisecondsSinceEpoch((json['dt'] ?? 0) * 1000,
              isUtc: true)
          : json['dt'],
      temp: TemperatureModel.fromJson(json['temp'] ?? {}),
      humidity: stringOrNull(json['humidity']),
      summary: stringOrNull(json['summary']),
      weather: List.of(json['weather'] ?? [])
          .map((e) => WheatherModel.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'dt': dt,
      'temp': temp?.toJson(),
      'humidity': humidity,
      'summary': summary,
      'weather': weather?.map((e) => e.toJson()).toList(),
    };
  }
}

class WheatherModel {
  String? id, main, description, icon;

  WheatherModel({
    this.id,
    this.main,
    this.description,
    this.icon,
  });

  factory WheatherModel.fromJson(Map<String, dynamic> json) {
    return WheatherModel(
      id: stringOrNull(json['id']),
      main: stringOrNull(json['main']),
      description: stringOrNull(json['description']),
      icon: stringOrNull(json['icon']),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'main': main,
      'description': description,
      'icon': icon,
    };
  }
}

class TemperatureModel {
  double? min, max, day;

  TemperatureModel({
    this.min,
    this.max,
    this.day,
  });

  factory TemperatureModel.fromJson(Map<String, dynamic> json) {
    return TemperatureModel(
      min: json['min'] is int
          ? (json['min'] as int?)?.toDouble()
          : json['min'] as double?,
      max: json['max'] is int
          ? (json['max'] as int?)?.toDouble()
          : json['max'] as double?,
      day: json['day'] is int
          ? (json['day'] as int?)?.toDouble()
          : json['day'] as double?,
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'min': min,
      'max': max,
      'day': day,
    };
  }
}

String? stringOrNull(dynamic value) {
  if (value != null) {
    return value.toString();
  }
  return value;
}
