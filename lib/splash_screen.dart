import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'home_page/home_page.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Future.delayed(
        const Duration(seconds: 2),
        () => Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const HomePage()),
            (_) => false));
    return Stack(
      alignment: Alignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/splash.gif',
              width: size.width,
              height: size.height * .5,
              fit: BoxFit.cover,
            ),
            Image.asset(
              'assets/splash.gif',
              width: size.width,
              height: size.height * .5,
              fit: BoxFit.cover,
            ),
          ],
        ),
        const Text(
          'Wheather App',
          style:
              TextStyle(color: Colors.white, decoration: TextDecoration.none),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: size.height * .01),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: SvgPicture.asset(
              'assets/logo.svg',
              width: size.width * .5,
            ),
          ),
        ),
      ],
    );
  }
}
