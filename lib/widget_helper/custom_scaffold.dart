import 'package:flutter/material.dart';

class CustomScaffold extends StatelessWidget {
  final bool isBusy, hasData;
  final Widget? body;
  const CustomScaffold(
      {super.key, this.isBusy = false, this.hasData = false, this.body});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.white],
                  transform: GradientRotation(.3))),
        ),
        Scaffold(
            backgroundColor: Colors.transparent,
            body: isBusy
                ? const Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 4.0,
                      color: Colors.blue,
                      backgroundColor: Colors.transparent,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                  )
                : body),
      ],
    );
  }
}
