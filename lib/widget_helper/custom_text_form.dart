import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormCustom extends StatelessWidget {
  final String? hintText, helperText;
  final String? Function(String? value)? validation;
  final void Function()? onTap;
  final Color borderColor, fillColor;
  final void Function(String value)? onChanged, onFieldSubmitted;
  final void Function(String? value)? onSaved;
  final void Function()? onEditingComplete;
  final TextEditingController controller;
  final TextInputType? keyboardType;
  final int maxLines;
  final bool readOnly, multilines, expands, fullBorder;
  final Widget? prefix, suffix;
  final FocusNode? focusNode;
  final double? height, width;
  final double textSize;
  final TextAlign textAlign;
  final EdgeInsetsGeometry? contentPadding;
  final FontWeight fontWeight;
  final TextStyle? style;
  final TextInputAction textInputAction;
  final List<TextInputFormatter>? inputFormatters;

  const TextFormCustom(
      {Key? key,
      this.hintText,
      this.helperText,
      required this.controller,
      this.onTap,
      this.expands = false,
      this.onEditingComplete,
      this.onChanged,
      this.onFieldSubmitted,
      this.onSaved,
      this.validation,
      this.borderColor = Colors.transparent,
      this.maxLines = 1,
      this.keyboardType = TextInputType.text,
      this.readOnly = false,
      this.prefix,
      this.suffix,
      this.fillColor = Colors.white,
      this.focusNode,
      this.height,
      this.width,
      this.fullBorder = false,
      this.textAlign = TextAlign.start,
      this.textSize = 16,
      this.contentPadding,
      this.fontWeight = FontWeight.w400,
      this.style,
      this.inputFormatters,
      this.textInputAction = TextInputAction.next,
      this.multilines = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (helperText != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Text(helperText!,
                  style: TextStyle(
                      color: Colors.grey.shade100,
                      fontSize: 14,
                      fontWeight: FontWeight.w400)),
            ),
          SizedBox(
            height: height,
            width: width,
            child: TextFormField(
              textAlign: textAlign,
              focusNode: focusNode,
              onEditingComplete: onEditingComplete,
              onSaved: onSaved,
              readOnly: readOnly,
              style: style ??
                  const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
              onTap: onTap,
              maxLines: multilines ? null : maxLines,
              expands: expands,
              keyboardType: keyboardType,
              inputFormatters: inputFormatters,
              textAlignVertical: TextAlignVertical.center,
              textInputAction: textInputAction,
              onChanged: (String value) {
                if (onChanged != null) {
                  onChanged!(value);
                }
              },
              onFieldSubmitted: onFieldSubmitted,
              controller: controller,
              decoration: inputDecoration(),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: controller.text.isNotEmpty
                  ? validation
                  : (value) {
                      return null;
                    },
              cursorColor: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }

  InputDecoration inputDecoration() {
    return InputDecoration(
      isDense: true,
      fillColor: fillColor,
      filled: true,
      hintText: hintText,
      prefixIcon: prefix,
      suffixIcon: suffix,
      focusedBorder: fullBorder
          ? OutlineInputBorder(
              borderSide: BorderSide(color: borderColor),
              borderRadius: BorderRadius.circular(10.0),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(color: borderColor),
              borderRadius: BorderRadius.circular(10.0),
            ),
      contentPadding: contentPadding,
      enabledBorder: fullBorder
          ? OutlineInputBorder(
              borderSide: BorderSide(color: borderColor),
              borderRadius: BorderRadius.circular(10.0),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(color: borderColor),
              borderRadius: BorderRadius.circular(10.0),
            ),
      hintStyle: TextStyle(
          color: Colors.grey.shade100,
          fontSize: 16,
          fontWeight: FontWeight.w400),
    );
  }
}
